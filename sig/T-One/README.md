## SIG主页

https://openanolis.cn/sig/t-one

## SIG目标
T-One兴趣组主要目标是建立OpenAnolis社区的质量保障基础设施(包括但不限于一站式的自动化测试平台T-One/Testfarm，Bisect缺陷定位工具等各类测试平台), 支持OpenAnolis社区的各类测试活动；此SIG组的主要活动有：
1. 探索业界在测试工具方面的优秀方案，同时结合社区在测试方面的需求，设计规划测试工具的后续方向。
2. 开发并维护相关测试工具，支撑社区的各类测试活动。
3. 开源相关测试工具，吸引社区的开发力量，并为社区开发者及合作企业提供测试服务。

## 平台链接
T-One：https://tone.openanolis.cn/

Testfarm：https://testfarm.openanolis.cn/


## 成员列表
| 成员  |  角色 |
| ------------ | ------------ |
| [yongchao](https://gitee.com/zy_chao)  |  maintainer | 
| vosamowho  | maintainer | 
| wjn740  | maintainer | 
| suqingming  | maintainer | 
| jacob2021 | maintainer | 
| fuyong  | maintainer | 
| wenlylinux  | contributor |
| zhangxuefeng  | contributor |
| wb-cy860729  |  contributor |
| jpt2021  |  contributor |
| woohello |  contributor |
| as461177513 | contributor |
| vosamowho |  contributor |

## SIG仓库

Source code repositories:
- https://gitee.com/anolis/testfarm
- https://gitee.com/anolis/testfarm-front
- https://gitee.com/anolis/tone-web
- https://gitee.com/anolis/tone-runner
- https://gitee.com/anolis/tone-agent
- https://gitee.com/anolis/tone-agent-proxy
- https://gitee.com/anolis/tone-agent-front
- https://gitee.com/anolis/tone-front
- https://gitee.com/anolis/tone-deploy
- https://gitee.com/anolis/tone-cli
- https://gitee.com/anolis/tone-storage

## 小组例会
双周会，采用线上会议形式

## 钉钉群

欢迎使用钉钉扫码入群

![](assets/dingding_group.jpeg)