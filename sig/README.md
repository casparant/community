SIG全称Special Interest Group，针对特定的一个或多个技术主题而成立。社区按照不同SIG组织，以便更好管理和改善工作流程。SIG成员主导SIG的治理、推动交付成果输出，并争取让交付成果成为社区发行的一部分。

欢迎任何人加入并参与贡献。您可以通过邮件列表与组内成员沟通，您在贡献的同时也将积累经验和提升影响力。

龙蜥社区 SIG 链接：https://openanolis.cn/sig

您可以在 SIG 主页审请创建 SIG，然后通过 PR 的形成提交到 community 仓库进行内容更新即可；社区官网 SIG 页面信息会与 community 仓库的 SIG 目录保持自动同步。

## SIG 目录规范
为了保证 SIG 内容能自劝同步到社区 SIG 主页，需要遵守以下规范填写 SIG 信息。

1. 一个SIG目录里主要有三块内容：README.md、sig-info.yaml 和 content，同步程序会通过 README.md 和 sig-info.yaml 这两个文件同步到社区SIG页面上(对应SIG基本信息), content里内容的目录跟SIG目录映射起来；同步程序会自动检测变更并同步到官网SIG。
2. sig-info.yaml 文件定义了 name，description, mailing_list, meeting_url，repositories 以及 maintainers、committers 等字段。
    - name 需要与 SIG 名称保持一致。
    - description 是 SIG 的描述信息。
    - mailing_list, meeting_url 是邮件列表及会议信息。
    - maintainers / contributor 字段中公司和name如果不想暴露的可以留空，openanolis_id 会同步到现在SIG页面的管理员字段上。
    - repositories 定义了 SIG 相关的仓库信息。

