# 双龙贺岁，龙蜥 LoongArch GA 版正式发布



### 简介

继  [**Anolis OS LoongArch 预览版**](http://mp.weixin.qq.com/s?__biz=Mzg4MTMyMTUwMQ==&mid=2247485709&idx=1&sn=bd231b79cbe1c4573758598b0162839e&chksm=cf66fa7ff81173692b514cd09ce3eb27e1515fc862b6ea30bc1c9e9f6768eeb2fb79c8769dfa&scene=21#wechat_redirect)发布后，现迎来龙蜥 LoongArch 正式版首发，该正式版在预览版的基础上提供了 AppStream、PowerTools 等仓库。Anolis OS 8.4 LoongArch 版是龙蜥社区发起的项目，完美地支持 LoongArch 体系架构，是打造国产化生态环境中重要的一项成果。

龙芯指令系统（LoongArch®）是龙芯中科基于二十年的 CPU 研制和生态建设积累推出的新指令集，具有较好的自主性、先进性与兼容性的新平台。包括基础架构部分和向量指令、虚拟化、二进制翻译等扩展部分，近 2000 条指令。

### 发布内容

Anolis OS 8.4 LoongArch 正式版发布产品包括 **ISO、软件仓库、虚拟机镜像、容器镜像**。

- ISO列表：
  *https://mirrors.openanolis.cn/anolis/8.4/isos/GA/loongarch64/AnolisOS-8.4-GA-loongarch64-dvd.iso* loongarch64架构的安装 ISO 镜像
- 虚拟机镜像列表：
  *https://mirrors.openanolis.cn/anolis/8.4/isos/GA/loongarch64/AnolisOS-8.4-GA-loongarch64.qcow2* loongarch64架构的虚拟机镜像
- 容器镜像列表：
  docker pull openanolis/anolisos:8.4-loongarch64
- 软件仓库列表：
  *https://mirrors.openanolis.cn/anolis/8.4/BaseOS/loongarch64/os/*
  *https://mirrors.openanolis.cn/anolis/8.4/AppStream/loongarch64/os/
  https://mirrors.openanolis.cn/anolis/8.4/PowerTools/loongarch64/os/*

### 亮点

- 支持图形界面和多种安装场景
- 使用 **docker-ce 20.10.3** 为默认的容器管理工具
- 使用 lbrowser 浏览器和 evolution 邮件客户端，lbrowser 基于 chromium 内核开发，支持 npapi 插件功能，支持国家商用密码算法模块和国产安全协议模块，修复目前已知所有安全漏洞。
- 内核更新到 **4.19.190-4**

### 硬件支撑

| **CPU **             | **内存 ** | **硬盘 **   |
| -------------------- | --------- | ----------- |
| `3a50003b50003c5000` | `4GB以上` | `120GB以上` |

### 已知问题

| 问题单 | 问题描述                                             |
| ------ | ---------------------------------------------------- |
| 67     | kdump 没有服务文件                                   |
| 84     | 内核没有 softlockup_panic、hardlockup_panic 等配置项 |
| 284    | rust 无法编译                                        |
| 305    | 内核没有提供 abi 包                                  |

### 镜像地址

#### 主机镜像：

*https://mirrors.openanolis.cn/anolis/8.4/isos/GA/loongarch64/*

#### 容器镜像：

```
docker pull openanolis/anolisos:8.4-loongarch64
```

#### 配置 EPEL 仓库：

```
cat > /etc/yum.repos.d/epel.repo << EOF
[epel]
name=epel
baseurl=http://pkg.loongnix.cn/loongnix-server/8.3/epel/loongarch64/release/Everything/
gpgcheck=0
EOF
```

#### 配置容器仓库：

执行以下命令编辑 /etc/docker/daemon.json，增加 insecure-registries 的配置，重新加载并重启 docker 使配置生效。

```
mkdir -p /etc/docker/
tee /etc/docker/daemon.json <<-‘EOF’
{
“insecure-registries”:[“harbor.loongnix.cn”]
}
EOF
sudo systemctl daemon-reload
sudo systemctl enable docker
sudo systemctl restart docker
```

使用以下帐号进行登陆：

用户名：loongsoncloud

密码：loongson@SYS3

```
docker login harbor.loongnix.cn
```

拉取镜像：

```
docker pull harbor.loongnix.cn/mirrorloongsoncontainers/alpine:v3.11.11
```

### 附录

Anolis OS 8 comes with no guarantees or warranties of any sorts, either written or implied. The individual packages in the distribution come with their own licences.  

### 致谢

衷心感谢参与和协助龙蜥社区（OpenAnolis）的所有成员，尤其是产品发布小组。特别感谢来自 LoongArch SIG 组的成员（https://openanolis.cn/sig/LoongArch）龙芯中科、统信软件、中科方德、万里红、红旗软件、阿里云是你们的辛勤付出，以及对开源的热爱才保障版本顺利发布，也为龙蜥操作系统(Anolis OS) 8 更好地发展提供无限空间！

#### 反馈

Bug跟踪：https://bugzilla.openanolis.cn/

邮件列表：loongarch@lists.openanolis.cn

—— 完 ——
**加入龙蜥社群**

加入微信群：添加社区助理-龙蜥社区小龙（微信：openanolis_assis），备注【龙蜥】与你同在；加入钉钉群：扫描下方钉钉群二维码。欢迎开发者/用户加入龙蜥社区（OpenAnolis）交流，共同推进龙蜥社区的发展，一起打造一个活跃的、健康的开源操作系统生态！

![开发者社区.png](https://ucc.alicdn.com/pic/developer-ecology/8062001a6f314614a2c2d8509fda2ea3.png)

**关于龙蜥社区**

**龙蜥社区**（OpenAnolis）是由**企事业单位、高等院校、科研单位、非营利性组织、个人**等在自愿、平等、开源、协作的基础上组成的非盈利性开源社区。龙蜥社区成立于 2020 年 9 月，旨在构建一个开源、中立、开放的Linux 上游发行版社区及创新平台。

龙蜥社区成立的短期目标是开发龙蜥操作系统(Anolis OS)作为 CentOS 停服后的应对方案，构建一个兼容国际 Linux 主流厂商的社区发行版。中长期目标是探索打造一个面向未来的操作系统，建立统一的开源操作系统生态，孵化创新开源项目，繁荣开源生态。

[目前，**龙蜥OS 8.4**](http://mp.weixin.qq.com/s?__biz=Mzg4MTMyMTUwMQ==&mid=2247484141&idx=1&sn=36007cf929317488a7ae2c374d81ea46&chksm=cf66f19ff8117889bbed793dc8b30b9bf5e1250aa6657b7a5e5f0f05cc2182cd836474ba06cb&scene=21#wechat_redirect)已发布，支持 X86_64 、Arm64、LoongArch 架构，完善适配飞腾、海光、兆芯、鲲鹏、龙芯等芯片，并提供全栈国密支持。

欢迎下载：***https://openanolis.cn/download***

加入我们，一起打造面向未来的开源操作系统！

***[https://openanolis.cn](https://openanolis.cn/)\***